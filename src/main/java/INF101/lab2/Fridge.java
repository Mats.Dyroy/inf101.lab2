package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    private static final int CAPACITY = 20;
    private List<FridgeItem> _items = new ArrayList<FridgeItem>(CAPACITY);

    @Override
    public int nItemsInFridge() {
        return _items.size();
    }

    @Override
    public int totalSize() {
        return CAPACITY;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (_items.size() < CAPACITY)
        {
            _items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!_items.remove(item))
            throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        _items.clear();        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        var expired = _items.stream().filter(item -> item.hasExpired()).toList();
        _items.removeAll(expired);
        return expired;
    }
    
}
